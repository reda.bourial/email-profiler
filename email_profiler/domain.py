import sys
import os
import csv

domains = {}
path = os.path.dirname(__file__)+"/assets/domains.csv"

# read csv 
with open(path) as csvfile:
    reader = csv.reader(csvfile,delimiter=",")
    for row in reader:
        domains[row[0]] = row[1]

def get_domain_type(domain):
    return domains.get(domain,"PRO")