import sys
import os
import csv

path = os.path.dirname(__file__)+"/assets/roles.csv"

with open(path) as f:
    roles = []
    for line in f:
        roles.append(line.strip())
    roles = set(roles)

def is_role(part):
    return part in roles