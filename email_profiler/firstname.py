import json
import gender_guesser.detector as gender

detector = gender.Detector()

def search_firstname(part):
    string = str.title(part)
    for  i in range(len(string)):
        candidate = string[:len(string)-i]
        if detector.get_gender(candidate) != "unknown":
            return candidate.lower()
    return None

def search_firstname_from_end(part):
    candidate = part
    for  i in range(len(part)):
        candidate = str.title(candidate)
        if detector.get_gender(candidate) != "unknown":
            return candidate.lower()
        candidate = candidate[1:]
    return None