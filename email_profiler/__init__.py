import pathlib

from .profiler import EmailProfiler

email_profiler = EmailProfiler()

def profile(email):
    return email_profiler.profile(email)
