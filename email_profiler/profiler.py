from validate_email import validate_email
from .firstname import search_firstname, search_firstname_from_end
from .domain import get_domain_type
from .roles import is_role
import string


allowed = set(string.ascii_lowercase + string.ascii_uppercase)

def check_a_z(test_str):
    return set(test_str) <= allowed

class EmailProfiler:


    def validate_email(self,email):
        return validate_email(email=email)

    def get_parts(self,email):
        if not self.validate_email(email):
            raise "%s is not a valid email" % email
        parts  = [email.split('@')[0]]
        delimiters = ['!', '#', '$', '%', '&', "'", '*', '+', '-', '/', '=', '?', '^', '_', '`', '.', '{', '|', '}', '~']
        for d in delimiters:
            new_parts = []
            for p in parts:
                new_parts.extend(p.split(d))
            parts = new_parts
        return parts

    def get_candidate(self,part):
        candidate = search_firstname(part)
        from_the_end = search_firstname_from_end(part)
        if from_the_end and candidate:
            return candidate if len(candidate)> len(from_the_end) else from_the_end
        elif candidate:
            return candidate
        return from_the_end
        
    def search_for_role_based_email(self,email):
        local_part = email.split("@")[0]
        if is_role(local_part):
            return {
                    "rolebased": True,
                    "firstname": [],
                    "lastname":  [],
                    "initial": [],
            }
    
    def analyse_single_part_email(self,part):
        candidate = self.get_candidate(part)
        first_name = candidate if candidate else ""
        last_name = part.replace(first_name,"")
        if check_a_z(last_name):
            if len(last_name)>2:
                return {
                    "rolebased": False,
                    "firstname": [first_name] if first_name else [],
                    "lastname": [last_name] if last_name else [],
                    "initial": [],
                }
            return {
                    "rolebased": False,
                    "firstname": [first_name] if first_name else [],
                    "initial": [],
            }
        return { 
            "rolebased": False,
             "firstname": [first_name] if first_name else []
        }
    
    def analyse_multi_part_email(self,parts):
        first_names = []
        last_names = []
        initials = []
        for part in parts:
            candidate = self.get_candidate(part)
            if part and candidate and len(candidate)>len(part) - 4:
                first_names.append(candidate)
                if len(candidate) < len(part):
                    initials.append(part.replace(candidate,""))
            elif part and check_a_z(part):
                if len(part) <=2:
                    initials.append(part)
                else:
                    last_names.append(part)
        return {
            "rolebased": False,
            "firstname": first_names,
            "lastname": last_names,
            "initial": initials,
        }

    def get_email_pattern(self,email,profile):
        pattern = email.split('@')[0]
        for ln in profile.get("lastname",[]):
            pattern = pattern.replace(ln,"(LASTNAME)")
        for fn in profile.get("firstname",[]):
            pattern = pattern.replace(fn,"(FIRSTNAME)")
        for initial in profile.get("initial",[]):
            pattern = pattern.replace(initial,"(INITIAL)")
        return pattern + "@"+  email.split('@')[1]

    def profile(self,email):
        email = email.lower()
        role_based_email = self.search_for_role_based_email(email)
        parts = self.get_parts(email)
        if role_based_email:
            profile =  role_based_email
        elif len(parts) == 1 :
            profile = self.analyse_single_part_email(parts[0])
        else:
            profile = self.analyse_multi_part_email(parts)
        # add pattern
        profile["pattern"] =self.get_email_pattern(email,profile)
        # add domain profile
        profile["domain"] = get_domain_type(email.split("@")[1])
        return profile
 