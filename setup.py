from setuptools import setup, find_packages
from os import path

def readme():
    # Get the long description from the README file
    here = path.abspath(path.dirname(__file__))
    with open(path.join(here, 'README.md'), encoding='utf-8') as f:
        return f.read()

setup(
    
    name             = 'email-profiler',
    version          = '0.0.1',
    author           = 'Reda Bourial',
    author_email     = 'reda.bourial@gmail.com',
    description      = 'Profile email addresses',
    long_description = readme(),
    long_description_content_type = 'text/markdown',
    license='LICENSE.txt',
    url = 'https://gitlab.com/reda.bourial/email-profiler',
    packages = find_packages(),
    keywords = 'social engineering email profiling',
    install_requires=[
        "gender_guesser",
        "validate_email",
    ],
    package_data={'email_profiler': ['assets/*.csv']},
    python_requires = '>=3',

    classifiers=[
        'Development Status :: 5 - Production/Stable',
		'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
    ],
    project_urls={
        'Bug Reports': 'https://gitlab.com/reda.bourial/email-profiler/-/issues',
    },
    py_modules=['email_profiler'],
)