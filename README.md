Profile the pattern of an email address.
# How to install : 
```
    pip install git+https://gitlab.com/reda.bourial/email-profiler.git
```
# How to use :
```
    import email_profiler
    email_profiler.profile("hello@ibm.com")
    >>> {'rolebased': True, 'firstname': [], 'lastname': [], 'initial': [], 'pattern': 'hello@ibm.com', 'domain': 'PRO'}
    email_profiler.profile("steve@apple.com")
    >>> {'rolebased': False, 'firstname': ['steve'], 'initial': [], 'pattern': '(FIRSTNAME)@apple.com', 'domain': 'PRO'}
    email_profiler.profile("michelleobama@gmail.com")
    >>> {'rolebased': False, 'firstname': ['michelle'], 'lastname': ['obama'], 'initial': [], 'pattern': '(FIRSTNAME)(LASTNAME)@gmail.com', 'domain': 'FREE'}
```